# Book_AcousticAnalysis

Acoustic Analysis for Marine Biologists
---

A walk through and reference guide for biologists working with acoustics. 

Download the latest version of the pdf [here](https://gitlab.com/RTbecard/book_acousticanalysis/-/jobs/artifacts/master/file/book_AcousticAnalysis.pdf?job=compile_pdf).
