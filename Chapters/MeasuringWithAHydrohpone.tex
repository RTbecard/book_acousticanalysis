%!TEX root = ../book_AcousticAnalysis.tex

Here we cover the basic protocols for measuring and analysing acoustic data.
This chapter is for those who are new to acoustic analysis.
In addition to providing a conceptual overview, we'll also provide examples in the form of reproducible R code and output.

\section{Measuring with a Hydrophone}

Here we'll cover the general steps involved with measuring underwater acoustic pressure:

\begin{itemize}
	\item Metadata and file structure when collecting data.
	\item Extracting absolute pressure measurements from recordings.
	\item Archiving acoustic data.
\end{itemize}

\subsection{File Structure and Metadata}

Before making any measurements, it's important to note down the settings of your recorder and your acoustic environment.
By noting down all settings that affect your recording, this \textit{meta checklist} will serve as a tool to avoid errors during data collection.
For the recorder, important settings to note are:
\begin{itemize}
	\item \textbf{Mic Gain}.  Turn this off if possible, as it will increase the complexity of calibrating your measurements.
	\item \textbf{Input port} (Line in vs.\ Microphone in).  When given the choice, you should choose line in.  In the case of using a hydrophone that is phantom powered, then you likely have to use the mic input of the recorder.
	\item \textbf{Recording level} (i.e.\ Recording volume).  
		Set this to a level to optimize your dynamic range.
		This means that you should always select the highest possible recording level which does not result in clipping (some recorders call clipping \textit{peaking} and have a visual indicator on the display to indicate when peaking occurs).
		All sounds which have a peak pressure greater than or equal to \SI{0}{\decibel} will exceed the voltage range of your recorder and this will produce unwanted acoustic artefacts in your recordings.
	\item \textbf{Automatic gain}.  Some recorders have options to dynamically adjust the gain while recording.  You'll need to check that your recorder has this feature disabled as this will make it impossible to calibrate your resulting files.
	\item \textbf{Model of recorder}.
	\item \textbf{File format}.  
		You should always use a lossless format when recording.  
		Selecting \texttt{wav} files are a safe bet as this is a widely used format.
	\item \textbf{bit depth}.
		This is also referred to \textit{bit rate} and \textit{dynamic range}.  You can typically choose between 16 and 24 bits.
		We suggest taking the highest value possible as this will increase the accuracy of your recordings.
		Selecting a high bit depth is particularity important when recording sounds which vary between high and low volume levels.
		A high bit depth will make sure that the low volume sounds are recorded accurately.
	\item \textbf{Preamp battery voltage}.
		When recording loud sounds, if your preamp battery has a low voltage your sounds run the risk of clipping via your hydrophone rather than on your recording divide.
		Recording the voltage before each recording session is a sure way to avoid this problem.
		For HTI-96 hydrophones which typically require a 9V battery to power the preamp, we suggest only using batteries which still hold a voltage above \SI{8.5}{\volt} when starting a recording session.
		For phantom powered devices, make sure you have fully charged your recorder batteries before each session.
\end{itemize}

With respect to your acoustic environment, you'll want to note down attributes which affect sound propagation.
Sound propagation, particularity in shallow water, is rather complicated so we'll focus on environmental attributes which have rather large effects.
We suggest noting the following down:

\begin{itemize}
	\item \textbf{Water Depth}.  Water depth plays a large role in retraining how quickly sound attenuates in addition to what the cut-off frequency is in your experimental area.
	\item \textbf{Sediment type}.  This is hard to describe, as the sediment on the surface my be quite different than what is a few centimetres below.
		However, we suggest noting this down if possible (sand, silt, clay, etc.).
		Like water depth, sediment type defines your cut-off frequency and attenuation rate.
		If you find large unexpected differences between recordings, a differing sediment type may be the cause.
	\item \textbf{Tidal phase}.  This can be retroactively noted down by checking your local tide schedules.
		In particular, if you are making recordings during low tide, this will result in relatively poor propagation conditions within your environment.
		As a result, during low tide, your measurements will be under-represent typical sound levels for that environments (from the perspective of abiotic propagation conditions).
\end{itemize}

When making measurements with your hydrophone, its important to define a strict file naming and metadata collection scheme.
We suggest the following:
\begin{itemize}
	\item Format: \textbf{yyyymmdd-hhmmss.wav}
	\item Example: \textit{20170101-131512.wav}
\end{itemize}

Most sound recorders have naming options to automatically name files according to the date/time of recording, making this a easy naming convention to follow when collecting data.

When recording your metadata, you can then make a table to link the times of your recordings to the appropriate experimental metadata.
Here's a rough example of how we record our metadata:

\begin{tabularx}{\textwidth}{X | X | X | X | X}
	\toprule
	Prefix & Suffix & Rec.level & Site & Depth (m) \\
	\midrule
	20120101 & 130512 & 4 & A & 2 \\
	- & 130734 & 4 & A & 4 \\
	- & 130942 & 4 & B & 2 \\
	\bottomrule
\end{tabularx}

\subsection{Collecting Acoustic Data}

Making hydrophone recordings is pretty straight forward.
Here we present a few points to keep in mind while making recordings.

\begin{itemize}
	\item Starting a recording session
		\begin{itemize}
			\item When starting a recording session, make sure to note down all metadata covered in the previous section.
				In addition to helping you interpret your results later, this will also serve as a checklist for avoiding mistakes.
		\end{itemize}
	\item Making recordings
		\begin{itemize}
			\item \textbf{Do not touch the hydrophone cable during recording}.
				Hydrophones are particularity sensitive to cable movement as compared to microphones.
			\item \textbf{Use headphones to monitor recordings}.
				A lot of things can go wrong from incorrect recorder settings to faulty hydrophone cables.
				If you use headphones to monitor your measurements while you take them, this is the best way to avoid measurement errors.
			\item \textbf{Make sure no clipping occurs during recording}.  Clipping occurs when your recording exceeds \SI{0}{\decibel FS}.
				Most recorders indicate this with a red light labelled \textit{peak} or \textit{clip}.
		\end{itemize}
\end{itemize}

\subsection{Convert Recordings to Absolute Pressure}

When you make a recording, it'll be saved as a digital waveform on your device.
We commonly report these values as \si{\decibel FS}.
This is just a decibel with a reference set as the maximum possible value that can be digitally recorded by the device, hence this is called \textit{decibel full scale}.
A measurement of \SI{0}{\decibel FS} represents the maximum of your amplitude range.
Your calibration constant is just a value which you use to convert from \si{\decibel FS} to \si{\decibel} (re \SI{1}{\micro\pascal})

To convert to absolute pressure, you'll need the calibration constant for your recording system.
This will be in the units of \si{\decibel} re \si{FS \per \micro\pascal}.

Applying the constant consists of:
\begin{enumerate}
	\item Load your sound file and normalize it to \si{FS} units.
	\item Apply your calibration constant.  You can multiply or add to do this, depending if your calibration constant is in the linear or \si{\decibel} scale.
\end{enumerate}

The following code block shows a streamlined example of the process:

\begin{rcode}
## Load file into R
data.boat.raw <- read.wav("./ExampleAudioRecording.wav")
## Set calibration constants
calib.recorder <- 15.28 # dB re V/FS
calib.hydro <- -164 # dB re V/uPa
calib.system <- calib.hydro - calib.recorder # dB re FS/uPa
## Calibrate File
data.boat.calib <- calibrateSignal.Time(data = data.boat.raw[[1]],
	calib = calib.system)
## Calculate SPL from calibrated sound
SPL <- 20*log10(rms(data.calib.ez)) 
\end{rcode}

\subsection{Archiving Acoustic Data}

Acoustic data requires a lot of storage space as we keep lossless formats.
An important step in post-data-collection is archiving your acoustic data.
For this purpose, we suggest two different options.
\begin{itemize}
	\item \textbf{zip}: This is a common format for all computers which has a reasonably good compression ratio.
	\item \textbf{7z}: While inconvenient on Mac, this is a common format on linux and windows.
		This has the highest compression ratio of all compression algorithems, but it comes at the cost of speed.
		If you have large amount of data and you don't need frequent access the data with a mac, we suggest this format.
\end{itemize}

For large collections of recordings, BASH scripts can be used to automate archiving commands.
The line below provides an example of saving all files with a matching time stamp into a zip archive.

\begin{bashcode}
find . -name "*20170101*" | zip -@ -9 20170101_DailyMeasurements.zip -@
\end{bashcode}

\section{Spectral Analysis}

As almost all acoustic analyses are dependant on frequency in some way or another, it's important to understand how audio signals are projected from the time domain to the frequency domain.
The term frequency domain simply refers to viewing the content of a signal with respect to frequency, as opposed to time.
Power spectral density or spectrogram plots are common frequency domain representations of signals.
This section will deal with the process of executing this projection and provide context to the interpretation of measures derived from the frequency domain.

\subsection{Discrete Fourier Transformation}

The Fourier transform lies at the heart of signal processing.
Any periodic signal with $N$ samples can be reconstructed using $N$ weighted sine functions across a spectrum of frequencies.
This allows us to project time domain information into the frequency domain.
If $\mathbf{x} = \{x_0, x_1, \mathellipsis, x_{N-1}\}$ is a vector of pressure measurements over time from some recorded signal, the discrete Fourier transform can be written as:

$$ \mathbf{X}_k = \frac{1}{N} \sum^{N-1}_{n=0} x_n \cdot e^{-i\cdot n \cdot 2\pi \frac{k}{N}}$$

This may look a bit abstract to those who are new to the concept.
A more intuitive way to write this is to apply Euler's formula $e^{ix} = \cos(x) + i \sin(x)$ and rewrite it with respect to trigonometric functions you're more familiar with.

$$ \mathbf{X}_k = \frac{1}{N} \sum^{N-1}_{n=0} \left( x_n \cdot \left[ \cos \left(2 \pi n \frac{k}{N} \right) - i\cdot \sin \left( 2\pi n \frac{k}{N} \right) \right] \right) $$

\begin{figure}
	\input{./Images/FourierTransform.tex}	
\end{figure}

Here, the term $k \in [0, N-1]$ determines the frequency being examined.
$k$ can be converted to hertz with $$\text{Hz} = k\frac{\text{Fs}}{N}$$ where Fs is the sample rate of the signal.
Fig.~\ref{fig:fourierPipeline} provides a visual guide to how the Fourier transform projects signals to a circular domain to generate averaged complex values for each frequency.
The complex modulus of these resulting values in $\mathbf{X}_k$ represent the spectral density of each frequency in the signal.

If we plot the modulus of $\mathbf{X}_k$ from \ref{fig:fourierPipeline} and convert our $k$ values to \si{\hertz}, we may see something like fig.~\ref{fig:DFTresults}.
Note that after we reach the Nyquest frequency (half the sample rate), we start mirroring our previous estimates, hence the negative frequencies.
This is the conjugate symmetric property of Fourier transformations.
To present the results as a one-sided spectrum, simply remove the negative frequencies and multiply the remaining y-axis values by 2.

\begin{figure}
	\input{./Images/DFTPlots.tex}
\end{figure}

\begin{figure}
	\input{./Images/perodicSignal.tex}
\end{figure}

It's important to note that as the Fourier transform works in a circular domain, it assumes periodic signals.
This is an assumption that is never met in practice and simply means that the signal consists of a full cycle of a repeating pattern.
Fig~.\ref{fig:periodicity} provides an illustrated guide to what we mean by periodicity.
Artefacts resulting from applying a DFT to a non-periodic signal are termed spectral leakage.
They cause other frequencies not present in the signal to appear in the resulting Fourier transform.
Hence, recorded signals analysed with a with the discrete Fourier transformation are inherently biased due to spectral leakage.

\subsection{Power Spectral Density}

As mentioned, measuring real signals inevitably results in violating the DFT assumption of periodicity.
To reduce the effects of spectral leakage on our estimates of spectral density, we can apply windowing and averaging.

Windowing reduces spectral leakage by applying an envelope function to our measured signal which tapers the amplitude toward the start and end of the signal.
This will result in a \textit{more} periodic signal at the cost of altering the information within signal, hence the choice of window function is important.
Two common choices are the \textit{Hann} and \textit{Hamming} windows.
\textit{Hann} windows are designed to minimize the spectral leakage across the whole spectrum while \textit{Hamming} windows offer less of a reduction of spectral leakage across the spectrum but provides more precise estimates of those frequencies present in the signal.
The choice of window type needs to be determined with respect to the purpose of the analysis.
Fig.~\ref{fig:windowing} shows DFT results with and without the application of a Hann window.

\begin{figure}
	\input{Images/windowing.tex}
\end{figure}

You may have noticed that the areas under each line in the DFT plot of fig.~\ref{fig:windowing} are equal.
This is termed \textit{Parseval's theorem}.
When we apply a DFT to a signal, the resulting energy projected onto the frequency domain must be equal to the energy found in the original time domain.
Hence, high degrees of spectral leakage can lead to underestimates of the true spectral densities of frequencies (peaks in the DFT will be underestimated more severely) as the spectral energy from the true frequency is incorrectly attributed to other frequencies.

As a consequence of windowing, information located near the beginning or end of our signal is going to be under-represented in the spectral density estimates.
With ideal stationary signals this isn't much of a concern, but when dealing with noisy, non-stationary signals this needs to be addressed. 
A popular routine to control for this problem is:

\begin{enumerate}
	\item Split the signal into multiple small windows which overlap by 50\%.
	\item Apply a window function to each of these window segments to reduce the spectral leakage.
	\item Average the DFT results across all window segments.
\end{enumerate}

This protocol for estimating spectral density is termed \textit{Welch's} method and is the kernel of all power spectral density (PSD) and spectrogram plots reported in the bioacoustic literature.
By choosing smaller window sizes, you are reducing the number of samples in the DFTs and thus reducing the frequency resolution of the resulting PSD plot.
However, smaller window sizes allow averaging across more window segments which results in a smoother plot providing a more accurate description of the spectral power.

\begin{figure}
	\input{./Images/WelchsSpectralDensity.tex}
\end{figure}

Converting our spectral density estimates to PSD values is simple as we just need to convert from pressure (root power quantity) to power by squaring.
We can then report our PSD values as decibels (see Eqn.~\ref{eq:dB}).
Fig.~\ref{fig:flowchartWelch} shows an example of calculating a PSD plot via Welch's method.
If we want to represent our measured signal as a spectrogram rather than a PSD plot, instead of averaging our PSD values over time we simply calculate the PSD values for each window segment and plot them with respect to time and frequency.

\section{Continuous vs Impulsive Sounds}

%% Venn diagram of acoustic metrics
\begin{figure}[H]
\begin{tikzpicture}
	\newcommand{\offset}{5cm}
	\newcommand{\radius}{5cm}
	\newcommand{\offsetlabel}{2.5cm}
	\newcommand{\opacity}{0.3}
	
	%% Circles
	\draw[fill=Apricot, fill opacity=\opacity, draw opacity=0] 
		(0,0) circle [x radius=\radius, y radius=\radius /2];
	\draw[fill=BlueGreen,fill opacity=\opacity, draw opacity=0]
		(\offset,0) circle [x radius=\radius, y radius=\radius /2];
	% Make 3 part rectangles for nodes
	\tikzset{sideNode/.style={rectangle, draw, align=center, draw opacity=0}}	
	\tikzset{centerNode/.style={rectangle, draw, align=center, draw opacity=0}}	

	% Example nodes
	\node[sideNode,label=\underline{Continuous}] at 
		(0 - \offsetlabel,0) (Continuous) 
		{Power Spectral Density\\
		Sound Pressure Level\\
		Fractional Octave Banded SPL};
	\node[sideNode,label=\underline{Transient}] at 
		(\offset + \offsetlabel, 0) (Transient)
		{Energy Spectral Density\\
		Peak-to-Peak Pressure\\
		Kurtosis\\
		Rise time};
	\node[centerNode,label=\underline{Both}] at 
		(\offsetlabel, 0) (Both)
		{Sound Exposure Level\\
		Fractional Octave Banded SEL\\
		Signal-to-noise Ratio};
\end{tikzpicture}
\caption{Venn diagram outlining which acoustic metrics are suitable for which types of sounds.}\label{fig:VennAcousticMetrics}
\end{figure}

\subsection{Energy vs. Power}

Generally, continuous sounds are described using power while transient sounds are described with energy.
Power is just the energy of a signal divided by the duration (i.e. Power is the derivative of energy with respect to time).

To support this statement, we'll illustrate a brief example.

\rcodefile{./Code/MeasuringWithAHydrophone/ContinousVsTransientMeasurments.R}
\noindent
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SimBoat}
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SimPulse}

Broadly speaking, above a certain threshold of your recording duration, continuous sounds should have a consistent power.
The same applies for transient sounds with respect to energy.
Above we've written a script that makes a continuous simulated boat and a transient Ricker pulse.
We'll report the power and energy of each simulated signal and make repeated measurements of both SEL and SPL with an increasing measurement duration. 

\rcodefile{./Code/MeasuringWithAHydrophone/ContinousVsTransientMeasurments2.R}
\noindent
\includegraphics[width=\linewidth]{./Code/MeasuringWithAHydrophone/SPLvsSEL}

As you can see, when the measurement duration is at 1 second SEL and SPL are equal on both tracks.
As the sample duration increases, SPL and SEL begin to diverge.
As we increase the window of our SPL calculation the level steadily decreases.
Similarly, as we increase the measurement period of our boat recording the SEL steadily rises.
This is not surprising, as the longer the signal, the more acoustic energy the boat releases.

It should now be clear that the choice of reporting power (SPL) or energy (SEL) is centred around which measurement provides us with an unbiased representation of the sound with respect to the measurement period.
If the results in the above plot don't quite make sense to you, it is strongly advised to refer to section \ref{commonMetrics} to review the math underlying SPL and SEL calculations.

\subsection{Time Domain Analysis}

When we say time domain, we are simply referring to a time-series of instantaneous pressure measurements with a constant time interval between each measurement.
With respect to continuous sounds, there are not any time domain exclusive metrics for us to apply, as by their nature, continuous sounds are relatively stable over time.
When we are examining transient sounds, we have a selection to choose from.
Below is a brief list of time domain measurements where we'll describe the biological relevance of each.
\begin{itemize}
	\item \textbf{Peak-to-peak pressure}: This is a standard metric for the effects of transient sounds on marine mammals.
		This has been related to hearing threshold shifts (\hl{get reference}) and can be thought of as a good representation of the maximum force applied to sensory hair cells during a pulse.
		A similar measure is zero-to-peak pressure, although this is rather uncommon in the biology literature as its a poorer indication of hair cell damage.
	\item \textbf{Kurtosis}: Kurtosis is a measure that is commonly used to describe the density of extreme values in a distribution.
		In the field of underwater acoustics, it may be applied to describe how impulsive a repeated transient sound is.
		An example of using kurtosis would be as a proxy for the density and activity of snapping shrimp within a reef.
		Here, kurtosis can be used to describe how the snapping shrimp change the distribution of the sound field.
		An calm reef with quiet background noise and loud shrimp sounds will result in a relatively low kurtosis value.
		From a hearing damage perspective, if two sounds have the same SPL but different kurtosis values, the sound with the lower kurtosis would be expected to be more damaging to hearing organs due to the higher incidence of extreme pressure fluctuations.
		As this metric is not very common, there are no standardized guidelines for how to measure it yet.
	\item \textbf{Rise-time}
\end{itemize}

\subsection{Frequency Domain Analysis}

If we apply a Fourier transformation to a periodic time-series, we end up with a signal in the frequency domain which has the same amount of energy and samples as its time domain representation.
This is the concept that underlies all the spectral analysis methods we'll be dealing with.
We won't get into the math here, but rather just give a brief visual overview of different ways to report frequency domain data.

\rcodefile{./Code/MeasuringWithAHydrophone/ContinousVsTransientMeasurments3.R}

As we mentioned earlier, transient sounds should be reported as energy and continuous as power.
This also applies to the frequency domain.
Using the plots from the above code block, we've plotted the Power Spectral Density and Energy Spectral Density of the continuous and transient sound, respectively.

\noindent
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraBoat}
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraBoat2}

For the PSD plots, it's also informative to represent the distribution of the windows for which each frequency was averaged over.
Continuous sounds which fluctuate a lot in amplitude over time will be represented by having wider percentile intervals and the mean PSD will be elevated more above the median PSD.
For PSD plots in which you've set a relatively low window length (resulting in a lower frequency resolution, but less variable mean estimates), you can also represent them with boxplots as seen above.
This gives a more detailed picture of the distribution, but for obvious reasons, this becomes impractical if you have a high frequency resolution.
It is up the researcher to choose a representation that best conveys the information relevant to the reader.

When plotting ESD, the acoustic energy is summed over all window segments rather than averaged.
For this reason, there are no percentiles or boxplots on ESD plots.

\noindent
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraBoat}
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraBoatOctave}\\
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraPulse}
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/SpectraPulseOctave}

Finally, if you're more interested in reporting your spectra in a manner that relates to the perception of sound, you can divide your signal into octave intervals which approximate the critical bandwidths of a particular species and sum all the power/energy within each band.
The critical bands of species are typically defined as partial octaves (as opposed to on a linear scale).
If possible, you should choose an octave fraction which is the actual critical band of your study species.
However, as this information is not known for a majority of species, you can choose 1/3 intervals when in doubt.

Note that the interpretations of Spectral Density and Fractional Octave Level plots are dramatically different.
Notice the difference in peak frequency when examining the plots for the simulated Ricker pulse.
The fractional octave SPL should be interpreted as the amount of acoustic energy that is exposed to a sensory hair cell.
This has obvious applications for research on permanent and temporary hearing threshold shifts.
As seen in the above figures, octave plots show values that are \textbf{not normalized with respect to frequency bin size}.
Since the size of the octave bins increase with respect to frequency, they are positively biased at higher frequencies.
To emphasize this, below we've plotted both PSD and octave banded SPL of a white noise (flat spectrum) signal.

\rcodefile{./Code/MeasuringWithAHydrophone/PSDvsSPLOctaves.R}

\noindent
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/GaussianPSD}
\includegraphics[width=0.5\linewidth]{./Code/MeasuringWithAHydrophone/GaussianSPL}

As expected, the plots show dramatically different trends in the provided signal.
When trying to convey information about the signal without taking perception into account, the PSD plot shows a normalized, unbiased representation of the spectra.
Here, we can clearly see that the acoustic energy is flat across all frequencies.
When viewing this from a perceptual standpoint, we are more concerned about how much energy is contained in the critical band of the species.
The critical band can be interpreted from an anatomy perspective as: the resonating bandwidth of the acoustic sensory hair cells of an animal.


\subsection{Signal-to-Noise Ratio}



\section{Measuring with a Vector Sensor}
\section{Estimating pressure gradients with Paired Hydrophone}

