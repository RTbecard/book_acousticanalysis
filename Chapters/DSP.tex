%! TEX root = ../book_AcousticAnalysis.tex

This chapter contains a number of self-contained articles.
Each will cover a concept or practice related to the measurement or processing of acoustic data.
The aim of this chapter is to supplement the practical material learned in the first two chapters with in-depth explorations of important related concepts.
This chapter is intended for readers seeking practical knowledge to develop or modify their own analytic tools.

\section{Resampling: Sinc Interpolation}



\section{Ambiguity Spectrum}

\section{Fourier Synthesis of Propagation Model Results}

Broadband computational models typically return a matrix of Green's function solutions (i.e. The impulse response of the modelled propagation environment) in the frequency domain with respect to spatial locations.
Propagation loss plots are generated from these solutions.
For a more tangible representation of propagation results, one can use these propagation solutions to reconstruct the time domain of the propagated signal at a specific receiver location.
This process is referred to as Fourier Synthesis and involves convolving the Green's function of a particular location with a source signature.
Jensen (2011) describes Fourier synthesis as:

\begin{align}
	p(r,z,t) &= \frac{1}{2\pi} \int^\infty_\infty p(r,z,\omega) e^{-i \omega t} \cdot d\omega \\
	p(r,z,t) &= \frac{1}{2\pi} \int^\infty_\infty S(\omega) \cdot \psi(r,z,\omega) e^{-i \omega t} \cdot d\omega 
\end{align}

~where $r$, $z$ are the range and depth of the receiver and $\omega$ is the angular frequency and $t$ is the instantaneous time.
$p$ is the simulated received signal, $\psi$ is the matrix of Green's functions solutions and $S$ is the fourier transform of the time domain source signal.
The operation is conceptually simple: Convolve the source signal with the impulse response to get the received signal.
In practice, there are some fine details you'll need to be careful about.

First, you'll need to resample your source signal to match the frequency domain of the propagation model.
Broadband propagation models normally require the user to specify the vertical fft length $n_f$, the bandwidth $B$ and often times the centre frequency of interest $f_0$.
We can calculate the matching time domain parameters with:

\begin{equation}
	\begin{split}
		T = \frac{n_f}{B}\\
		n_t = n_f\\
		\mathit{fs} = \frac{T}{n_t}
	\end{split}
\end{equation}

$T$ is the duration of the signal in seconds, $n_t$ the number of samples in the time domain signal, and $\mathit{fs}$ the matching sample rate of the time domain.
Using these parameters, you can use sinc interpolation to resample any source signal to $\mathit{fs}$ for use in your model.

When we use a Fourier transform to convert a time series into the frequency domain, this results in conjugate symmetric frequency values (i.e.\ a mirrored spectrum).
Propagation models, on the other hand, don't typically centre their frequency domains around \SI{0}{\hertz} but rather calculate results on a one sided spectrum where the lowest frequency will be greater than 0.
To convolve our source signature, we first have to apply a frequency offset so it matches the domain of our propagation solutions.
This can be done like so:

\begin{equation}
\begin{split}
	s_{\mathit{shift}}(\mathbf{t}) & = s(\mathbf{t})\cdot e^{i 2 \pi f_0 \mathbf{t}}	
\end{split}
\end{equation}

$s(t)$ and $s_{\mathit{shift}}(t)$ are the pressure time series of the original and frequency shifted source signals.
$\mathbf{t}$ is the vector of time values with respect to $s$.

Now we can execute the synthesis.
We'll write this in discrete time notation with respect to your resampled source signal $s$.

\begin{align}
	S_{shift}(\omega) &= \mathit{DFT}(s(t)\cdot e^{-i 2 \pi f_0 \mathbf{t}}) \cdot \frac{1}{n_f} \\
	p(r,z,t) &= \mathit{DFT}\left( S_{\mathit{shift}}(\omega) \cdot \psi(r,z,\omega) \right) e^{-i 2 \pi f_0 \mathbf{t}} 
\end{align}

$\mathit{DFT}$ represents a discrete Fourier transform.
Note the inverse frequency shift operator $e^{-i 2 \pi f_0 \mathbf{t}}$ applied again after the synthesis.
This is to remove the frequency shift from the propagation solutions mentioned earlier.
Also note $\frac{1}{n_f}$, normalizing our spectrum values with respect to the frequency resolution after applying the Fourier transform.
Finally, the resulting time series $p(r,z,t)$ will be complex as the propagation solutions were not conjugate symmetric when we applied the Fourier transform to them.
Simply take the real values of the resulting complex pressure series $\mathbb{R}(p(r,z,t))$ as the simulated receiver signal.

To verify the Fourier synthesis, you can generate PSD plots of your source signal and the simulated received signal at a close distance where you expect it to strongly resemble the source waveform.
If you applied your frequency shifts correctly, the PSDs should be in good agreement.
It may also be a good idea to run a test in shallow water where you expect to see clear modal dispersion.
It's possible that the dispersion effects can be applied along the opposite time direction if the matrix of propagation solutions is flipped the wrong way during synthesis.

\section{Bandpass filtering: Butterworth}
