\section{Acoustic Propagation 101}
	\begin{itemize}
		\item Fundamental Concepts
		\begin{itemize}
			\item Refraction (Snell's law)
			\item Diffraction: Huygens-Fresnel principle
			\item Attenuation
			\item Normal Modes
		\end{itemize}
		\item Mechanics of Propagation
		\begin{itemize}
			\item Shallow water: Normal Mode propagation
			\item Deep Water: SOFAR channel
		\end{itemize}
	\end{itemize}

\subsection{Fundamentals}

Here we'll briefly cover some key concepts required for understanding acoustic propagation.
We'll try to keep things conceptual by explaining with pictures, rather than providing mathematical explanations.
\subsubsection{Attenuation}

In marine sediments, attenuation is measured in \si{\decibel} with respect to the frequency you're working with.
Common units are either:

\begin{itemize}
	\item \si{\decibel \per \meter \per \kilo\hertz}
	\item \si{\decibel\per\lambda}
\end{itemize}

When applying models, it may be necessary to convert between these units.
The following proof provides the required conversion factor.

\begin{equation}
	\begin{split}
		\si{\decibel\per\meter\per\kilo\hertz} =& \si{\decibel} / \left(d \cdot \frac{v}{1000} \right)\\
		=& \si{\decibel} / \left(d \cdot \frac{c}{\lambda \cdot 1000} \right)\\
		=& \si{\decibel} / \left(\frac{c}{1000} \cdot \frac{d}{\lambda} \right)\\
		\frac{c}{1000} \cdot \si{\decibel\per\meter\per\kilo\hertz} =& \si{\decibel} / \left( \frac{d}{\lambda} \right)\\
		\frac{c}{1000} \cdot \si{\decibel\per\meter\per\kilo\hertz} =& \si{\decibel} / \lambda\\
	\end{split}
\end{equation}

where $d$ is the distance in meters the propagating wave has travelled through the sediment, $v$ is the frequency in \si{\hertz}, $\lambda$ is the wavelength in meters and $c$ is the speed of sound in the medium in \si{\meter\per\second}.

There are a wide variety of units used for reporting attenuation.
See table 

\begin{table}
	\caption{Constants you can use to convert between different attenuation units.
	Multiplying a value by the given constant will convert it from the unit listed in the column \textbf{from} to the unit in column \textbf{to}.
The constant $c$ refers to the sound speed in water.}
	\label{tab:attenuationConversion}
	\begin{tabular}{c c m{8cm}}
		\toprule
		From & To & Constant \\\midrule
		dB & Np & $20\cdot\log_{10}e \approx 8.686$ \\
		kyd & km & $\left( \frac{1}{3} \right) \cdot \left( \frac{1}{0.3048} \right) \approx 1.094$ \\
		\si{\decibel \per \meter \per \kilo \hertz} & \si{\decibel \per \lambda} & $\frac{c}{1000} \approx 1.5$ \\\bottomrule
	\end{tabular}
\end{table}

\section{Analytic Models}
	\begin{itemize}
		\item Geometric Models
		\item Sediment Interaction Models
		\begin{itemize}
			\item Ainsley 2010
			\item Bagocius 2018
		\end{itemize}
	\end{itemize}

\section{Computational Models}
	\begin{itemize}
		\item Overview of types of models.
		\item KrakenC: Shallow water, low frequency, very slow, very accurate, leaky Modes.
		\item MMPE: Shallow water, low frequency.
		\item Bellhop: No diffraction, high frequencies, deep water.
		\item Discrete Signal Processing 101
	\end{itemize}

\section{GIS: Acquiring Sediment and Bathymetry Covariates}

While the computational models allowing for range-varying sediment and bathymetry conditions are considerably more powerful, finding high resolution data suitable for use with these models can be challenging.
In the context of European marine waters, this data is freely available from the European Marine Data and Observation and Transfer Network (EMDOTnet).
This section will is intended for those new to GIS and will give a brief overview of key concepts followed by instructions on how to use this resource.

\subsection{GIS Concepts}

Broadly speaking, GIS data comes in two formats:
\begin{itemize}
	\item \textbf{Rasters}: Rasters are simply a rectangular matrix of data points with spatially referenced extents.
		You may think of a raster as a simple bitmap image where the first and last rows and columns of the image have associated latitude/longitude coordinates.
		Rasters are preferred when non-homogeneous high resolution data is required as the spatial complexity of the underlying data has no impact on the size of the resulting raster.
		When using rasters, be mindful about what projection you are working in.
		If you apply a raster to a map of a different projection, this will result in uneven spacing between raster cells that translates into a spatial bias of your spatial resolution.
		We suggest using \textit{GeoTIFF} images when working with rasters, as this is a common and easily accessible format.
	\item \textbf{Vector objects}: This class of data includes points, lines, and polygons.
		Unlike a raster, for each data value a new vector object must be created which describes the covered area and value of that data sample.
		This leads to very complex data taking up large amounts of space.
		Vector objects are very useful for describing more homogeneous data that can be categorized into groups/levels such as different types of habitat categories or elevation contours.
		We recommend working with \textit{ESRI shape files} when dealing with vector data do to its popularity.
\end{itemize}

Depending of the nature of your data (required accuracy, spatial complexity of the underlying process), one format is typically more appropriate than the other.

While the planet takes the shape of an ellipsoid, a lot of GIS work is conducted on 2D surfaces as this greatly reduces the complexity of the required analyses.
This is unsurprising.
When we analyse spatial regions on the scale of a few kilometres we tend not to factor in the effects of latitude on the curve of the Earth's surface.
Projecting from one spatial system to another is the kernel of any GIS analysis.
These spatial systems are termed \textbf{coordinate reference systems} (CRS).
There are far too many CRS formats to describe, but here we offer three popular choices that are commonly used.

\begin{itemize}
	\item \textbf{EPSG:4326}.  This is a global CRS based on the WGS84 ellipsoid (an approximation of the Earth's shape).  The units of this CRS are latitude and longitude in decimal degrees.
		This is a common CRS, even to those who are unfamiliar with GIS, and should be used when your dataset spans considerably large regions of the world (such as a continent or large country).
		Note that this is unsuitable for comparing distances at different parts of the world, as the units of decimal degrees are not consistently spaced with respect to latitude (1 decimal degree in Northern Canada is considerably different than 1 decimal degree in southern Canada).
	\item \textbf{WGS84/UTM}.  This is not a single CRS, but a family of CRSs.
		The Universal Transverse Mercator (UTM) is a 2D projection with Easting and Northing in meters (as opposed to latitude and longitude in degrees).
		You can select from a number of zones in UTM (such as 30N with gives an accurate projection of the United Kingdom) where each zone has a different reference longitude and latitude.
		In UTM, the important reference is longitude.
		As UTM is a cylindrical projection, all points along the North-South running reference line are scaled correctly.
		As you deviate East or West from this line, your projection presents less accurate scaling (features will seem larger than they should).
		When working in small geographic regions where a 2D CRS is preferred, this should be your first choice in CRS.
	\item \textbf{NAD83/UTM}.  Another very closely related projection is the \textit{North American Datum 83} based UTM projections.
		While the previous CRS family was optimized for having consistent accuracy around the world, this family of CRSs has been optimized for North America and hence should not be used outside of this region.
		That being said, the differences in accuracy between the two UTM systems is expected to be on the order of a few meters.
\end{itemize}

Always check the CRS of any data you receive or collect.
A common mistake for field technicians not well-versed in GIS is to report UTM coordinates (Easting and Westing) without providing the Zone they used.
As the Easting and Westing are defined as the distances in meters from the reference longitude and latitude of a given UTM zone, this information is meaningless without the full description of the CRS used to collect the data.

\subsection{Acquiring EMDOTnet GIS Layers}

EMDOTnet provides rasters for bathymetry data and polygons for sediment classes (Folk classification scheme with 16 categories for soft sediments).
First we'll start with acquiring the bathymetry data.

\begin{enumerate}
	\item Navigate to \url{http://www.emodnet.eu/}, select the bathymetry portal and locate the \textit{bathymetry viewing and downloading service}.
		This will provide you with a map where you can select tiles for which you'd like to retrieve a bathymetry raster.
		Select the regions you need and select \texttt{RGB GeoTIFF} as your preferred format.
		You should receive an email with in a minute or so with a download link to your data package.\\
		\includegraphics[width=0.7\linewidth]{Images/GIS/1}
\end{enumerate}

Next, we need to get our sediment classification covariates.
This process is a bit more complicated as we'll have to use a GIS application to connect to EMODnet's web feature service (WFS).
A WFS is just a method for conducting GIS analysis on vector data types without having the data locally on your machine.
In the interests of reproducible research, we suggest downloading the data locally form this server before conducting your analysis.
While R has powerful GIS tools, unfortunately there are currently no packages for making calls to WFS servers.
Instead we'll use QGIS, a graphical interface for the free GRASS GIS software suite.

\begin{enumerate}
	\item Download, install, and open QGIS from \url{https://www.qgis.org/en/site/}.
	\item Next, lets get the address of the WFS we need.
		At the time of writing, the EMDOTnet-Geology website listed it as \url{http://drive.emodnet-geology.eu/geoserver/EMODnetGeology/wfs}.
	\item Now that we have QGIS running, lets add the WFS to our project so we can access sediment data.
		In QGIS, navigate to \texttt{Later > Add Layer > Add WFS Layer} and use the URL from the previous step to add the sediment classifications to your map.\\
		\includegraphics[width=\linewidth]{Images/GIS/2}
	\item Note the coordinate system listed in the lower right of QGIS.
		Our project has adopted the coordinate system of the data we just loaded.
		Now is the time to change to the preferred CRS for your output data.
		Click on the CRS in the lower right of QGIS, enable \textit{on the fly CRS transformations}, and select the output CRS of your choice.
		Now the map you're looking at should be scaled correctly.
		Here, since we're working in the UK, I chose WGS84/UTM30N.\\
		\includegraphics[width=\linewidth]{Images/GIS/3}
	\item Now we'll extract the data we need.
		Using the on-screen coordinates telling where your mouse is pointing, determine the extents of the region you want to extract.
		Next, right-click on the sediment layer and select \texttt{Save As\textellipsis}.
		Now type in the extents of the region you want to extract.
		In the example image, I'm only extracting sediment data from Moray Firth in Northern Scotland.\\
		\includegraphics[width=0.7\linewidth]{Images/GIS/4}
\end{enumerate}

\subsection{Working with GIS in R}

Now we have the data, we need to pre-process it so we can use it in our propagation models.

