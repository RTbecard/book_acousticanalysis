%! TEX root = ../book_AcousticAnalysis.tex
\section{Decibels}
\begin{equation}
\label{eqDecibel}
10 \cdot \log_{10}\left(\frac{P}{P_0}\right) = dB
\end{equation}

\begin{flushright}
P = Power\\
dB = Decibel
\end{flushright}

A Decibel is a logarithmic ratio of \textit{power quantities} (\ref{eqDecibel}).  An example of a power quantity is \textit{Sound Intensity}.  As the pressure and particle velocity values which we commonly measure are \textit{root power} quantities, we can convert them into power quantities by squaring them.\\

Sound Pressure Level (dB re 1$\mu$Pa):
\begin{equation}
20 \cdot \log_{10}\left(\frac{p}{p_0}\right) = 10 \cdot \log_{10}\left(\frac{p^2}{p_0^2}\right) = \textrm{SPL}
\end{equation}\label{eq:dB}

\begin{lstlisting}[style=GitHub,language=r]
SPL = 20*log10(P/1)
\end{lstlisting}

Sound Velocity Level (dB re 1m/s):
\begin{equation}
	20 \cdot \log_{10}\left(\frac{u}{u_0}\right) = 10 \cdot \log_{10}\left(\frac{u^2}{u_0^2}\right) = \textrm{SVL} 
\end{equation}

\begin{flushright}
$p$ = \text{pressure} \\
$p_0$ = \text{reference pressure} \\
$u$ = \text{particle velocity} \\
$u_0$ = \text{reference particle velocity} \\
\end{flushright}

\begin{lstlisting}[style=GitHub,language=r]
SVL = 20*log10(u/1) = 10*log10(u^2/1)
\end{lstlisting}


\subsection{Convert between Decibels and Linear Units} \label{convertFromdBToLinear}
The proof below shows how to convert from Decibels to root-power quantities.  As an example, we're converting from SPL (dB ref \SI{1}{\micro \pascal}) to pascals (\si{\pascal})

\begin{equation}
\label{eq1}
\begin{split}
10 \cdot \log_{10}\left(\frac{p^2}{p_0^2}\right) &= \textrm{SPL} \\
\log_{10} \left( \frac{p^{20}}{p_0^{20}}\right) &= \textrm{SPL} \\
\log_{10}\left(\left(\frac{p}{p_0}\right)^{20}\right) &= \textrm{SPL} \\
10^{\textrm{SPL}} &= \left(\frac{p}{p_0}\right)^{20} \\
\left( 10^{\textrm{SPL}} \right) ^ {\frac{1}{20}} &= \left( \left( \frac{p}{p_0} \right) ^ {20} \right) ^ {\frac{1}{20}} \\
10^\frac{\textrm{SPL}}{20} &= \frac{p}{p_0} \\
p_0 \cdot 10^\frac{\textrm{SPL}}{20} &= p \\
\end{split}
\end{equation}

\begin{lstlisting}[style=GitHub,language=r]
p = 1*10^(SPL/20)
\end{lstlisting}

\section{Common Acoustic Metrics}\label{commonMetrics}

This section is a brief list of commonly used acoustic measurements using the dB scale.
While some of these have official ISO definitions, others (such as rise-time and signal-toinoise ratio) do not.
When reporting these values, be sure to check the ISO standards if your metric has been defined.
If it has not, make sure to define the metric in your manuscript.

\begin{itemize}
	\item Zero-to-peak sound pressure (\si{\micro \pascal})
		\begin{equation}
			p_{z \mhyphen p} = \textrm{max}( \mathbf{p} )
		\end{equation}

	\item Zero-to-peak sound pressure level (dB re \SI{1}{\micro \pascal}):
		\begin{equation}
		\textrm{SPL}_{z\mhyphen p} = 10 \cdot \log_{10}\left(\frac{p_{z\mhyphen p}^2}{p_0^2}\right)
		\end{equation}

		\begin{lstlisting}[style=GitHub,language=r]
		zeroToPeak = 20*log10(max(abs(p))/1)
		\end{lstlisting}

	\item Sound Exposure (\si{\micro \pascal \squared \second}) and Velocity Exposure (\si{(\nano \meter \per \second) \squared \cdot \second}):
		\begin{equation}
			\begin{split}
				\textrm{E}_\mathit{pressure} &= \sum_{i=1}^{N} \mathbf{p}[i]^2 dt\\
				\textrm{E}_\mathit{velocity} &= \sqrt{ \left( \sum_{i = 1}^{N}  u_x^2[i] \cdot dt \right)^2 + \left( \sum_{i = 1}^{N} u_y^2[i] \cdot dt \right)^2 + \left( \sum_{i = 1}^{N} u_z^2[i] \cdot dt \right)^2}
			\end{split}
		\end{equation}
		\begin{flushright}
			$\mathit{dt}$ = temporal resolution of signal (seconds per sample).
		\end{flushright}
		To calculate the single strike sound exposure ($E_{ss}$), set $t_1$ and $t_n$ to the occurrence of the $5^{th}$ and $95^{th}$ percentile of the cumulative energy of your recording.
		Of course, the duration of the clip you're analysing will have an effect on this metric, but so long as you have a strong signal-to-noise ratio, the resulting SE should be consistent, regardless of the duration of the clip you have.\\

	\item Sound Exposure Level (dB re \SI{1}{\micro \pascal \square \second}) and Velocity Exposure level (dB re \SI{1}{(\nano \meter \per \second) \squared \second}):
	\begin{equation}
		\begin{split}
			\textrm{SEL} = 10 \cdot \log_{10}\frac{E_\mathit{pressure}}{E_0}\\
			\textrm{VEL} = 10 \cdot \log_{10}\frac{E_\mathit{velocity}}{E_0}
		\end{split}
	\end{equation}

	\begin{lstlisting}[style=GitHub,language=r]
	SEL = 10*log10((sum(p^2)*dt)/1) #dt = sample period
	\end{lstlisting}

	\item Rise Time

\end{itemize}
\section{Calculate SPL from Power Spectral Density (PSD) results}

\begin{enumerate}
	\item Using \textit{Parseval's theorem}, we know the area under the curve of a PSD (the summed power values in the frequency domain) is equal to the variance of the signal (root-mean-square pressure in the time domain).
		Sum the power values across all frequencies and then multiple the result by $\mathit{df}$ (i.e.~the frequency resolution) to get the cumulative power of the PSD.

\begin{equation}
	\textrm{rms}(\mathbf{p})^2 = \sum_{i = 1}^{N}\mathbf{P}[i] df
\end{equation}

\begin{flushright}
$\mathbf{P}[i]$ = The $i$th element from a vector of power values.\\
$\mathbf{p}$ = A vector of pressure values.\\
$N$ = The length (number of samples) of your discreet Fourier transform (i.e.~window length of PSD).\\ 
$\textrm{rms}()$ = The root-mean-square of a given vector.
\end{flushright}

You can calculate $\mathit{df}$ by just looking at the difference between frequency values in your PSD results, or you may use the following equation:

\begin{equation}
	\mathit{df} = \mathit{fs}/N
\end{equation}

\begin{flushright}
	$\mathit{fs}$ = Sample rate of signal.
\end{flushright}
\begin{lstlisting}[style=GitHub,language=r]
P.sum = sum(Pxx)*df  #df = frequency interval
\end{lstlisting}

\item Convert to decibels.

\begin{equation}
	10 \cdot \log_{10}\left(\frac{\textrm{rms}(\mathbf{p})}{P_{0}} \right) = \mathit{SPL}
\end{equation}

\begin{lstlisting}[style=GitHub,language=r]
SPL = 10*log10(P.sum/1)
\end{lstlisting}

\end{enumerate}


\section{Predicting Particle Velocity from Pressure Measurements}

In the acoustic far-field, particle motion and sound pressure hold a theoretically constant relationship as sound behaves like longitudinal wave (as opposed to a spherical shaped wave in the near-field).
Later, we'll use this relationship as a baseline for examining deviations from \textit{"expected open water"} acoustic particle motion to pressure ratios.

\begin{equation}\label{eq:PFV}
  \mathit{PFV} = \frac{p}{\rho \cdot c}
\end{equation}

\begin{flushright}
	$p$ = Measured pressure (\si{\micro \pascal})\\
	$\mathit{PFV}$ = Predicted far-field velocity (\si{\meter \per \second})\\
	$\rho$ = Density of water (\SI{1027}{\kilogram \per \meter \cubed})\\
	\textit{c} = Speed of sound in water (\SI{1484}{\meter \per \second} )
\end{flushright}

To convert from the root-power PFV to a dB level ($\textrm{PFVL}$ dB re \SI{1}{\nano \meter \per \second}), we can do the following:

\begin{enumerate}
  \item Convert particle motion predictions to dB.

  \begin{equation}
	  20 \cdot \log_{10} \left( \frac{\mathit{PFV}}{u_0} \right) = \textit{PFVL}
  \end{equation}

\begin{lstlisting}[style=GitHub,language=r]
PFV = pressure/(density * c)
PFVL = 20*log10(PFV/1)
\end{lstlisting}

\end{enumerate}


\section{Working with Particle Motion}
\subsection{Particle Acceleration}

Particle acceleration, $a$, can be derived from particle velocity measurements, $u$.
The relationship between the two quantities is dependant on frequency, $f$, and is shown in the equation below.

\begin{equation}
  a = u \cdot \left( 2 \cdot \pi \cdot f \right)
\end{equation}

Note that $2 \cdot \pi \cdot f$ is the angular frequency, denoted by $\omega$ in other literature.

\begin{lstlisting}[style=GitHub,language=r]
a = u * 2 * pi * f
\end{lstlisting}

To report particle acceleration as a Decibel, use Particle Acceleration Level (PAL) with a reference value of \SI{1}{\micro \meter \per \second \squared}.

\begin{equation}
	\mathit{PAL} = 20 \cdot \log_{10}\left(\frac{a}{a_0}\right)
\end{equation}

\begin{lstlisting}[style=GitHub,language=r]
PAL = 20*log10(u * 2 * pi * f / 1)
\end{lstlisting}

\subsection{Comparing SVL to SPL}

As SPL is defined as the root mean square sound pressure in Decibel units, SVL is defined in the same way.

\begin{equation}
  \begin{split}
	  \textrm{rms}(u) &= \sqrt{\frac{1}{n} \sum_{i = 1}^{n}  \mathbf{u}[i]^2 \cdot dt}\\
    \mathit{SVL} &= 20 \cdot \log_{10} \left( \frac{\textrm{rms}(\mathbf{u})}{u_0} \right)\\
  \end{split}
\end{equation}

\begin{lstlisting}[style=GitHub,language=r]
SVL = 20*log10(rms(u)/1)
\end{lstlisting}

Particle motion is a vector quantity, as opposed to scalar.
To compare omni-directional pressure measurements to particle motion, we can measure the particle motion along three orthogonal directions and then sum the results.

\begin{equation}
  \begin{split} \label{eq:uxyz}
	  \textrm{rms} \left( \mathbf{u}_{xyz} \right) &= \sqrt{\textrm{rms} \left( \mathbf{u}_x \right) ^ 2 + \textrm{rms} \left( \mathbf{u}_y \right) ^2 + \textrm{rms} \left( \mathbf{u}_z \right) ^2}\\
	  \mathit{SVL} &= 20 \cdot \log_{10} \left( \frac{\textrm{rms} \left( \mathbf{u}_{xyz} \right) }{u_0} \right)
  \end{split}
\end{equation}

where $u_x$, $u_y$, and $u_z$ are vectors of the instantaneous particle velocity measured along the three axes of a vector sensor.

\begin{lstlisting}[style=GitHub,language=r]
SVL.xyz = 20*log10(sqrt(rms(u.x)^2 + rms(u.y)^2 + rms(u.z)^2)/1)
\end{lstlisting}

\subsection{Excess SVL}

If you'd like to describe the proportional relationship between particle motion and sound pressure, an intuitive way to do this is to compare the observed ratio of the two components to some sensible baseline.
As mentioned earlier (Eq.~\ref{eq:PFV}), in the acoustic far field there is a constant relationship between acoustic particle motion and sound pressure which is defined by the properties of the medium (density of, and speed of sound within the medium).
This will serve as our baseline for describing the proportional particle motion which we'll call \textit{excess particle motion} from here on.
We can then report this relationship in the Decibel scale so it can be intuitively compared to SPL and SVL measurements ($\textrm{SVL}_{\mathit{Excess}}$).
We'll do this in three steps:

\begin{enumerate}
	\item Measure both the particle motion summed across the three axes ($u_{\textrm{xyz}}$)  and sound pressure ($p$) in your setup.
	\item Calculate the predicted far field velocity ($\textrm{PFV}$).  This is the expected amount of particle motion for a given pressure measurement in open water, far field conditions.
	\item Subtract the expected ($\textrm{PFV}$) particle motion form the measured ($u_{\textrm{xyz}}$) and report the result as a decibel.
\end{enumerate}

The following equation will give the ExcessSVL:

\begin{equation}
  \begin{split}
	  \mathit{SVL}_{Excess} &= \left(20 \cdot \log_{10} \left( \frac{ u_{xyz}}{u_0} \right) \right) - \left( 20 \cdot \left( \log_{10} \frac{\mathit{PFV}}{u_0} \right) \right) \\
	  \mathit{SVL}_{Excess} &= 20 \cdot \log_{10} \left( \frac{ u_{\textrm{xyz}} }{\mathit{PFV}} \right) \\
  \end{split}
\end{equation}

See equations~\ref{eq:PFV}~\&~\ref{eq:uxyz} for calculating the necessary quantities used in the equation.
If the resulting $\textrm{SVL}_{Excess}$ is 3 $\textrm{dB}$, this means that the observed particle motion summed across all three axes is 3 $\textrm{dB}$ higher than what would be observed in a far-field, open water condition where an equivalent sound pressure has been observed.
This is a useful metric of you want to compare the acoustic conditions of a tank or basin setup to the theoretical conditions of the open water.
The $\textrm{SVL}_{Excess}$ can be applied to any acoustic metric in which both pressure and particle velocity can be substituted such as: SVL, VEL and $\textrm{SVL}_{z\mhyphen p}$. 

