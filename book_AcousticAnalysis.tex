\documentclass[11pt]{book}
%%%%%%%%%%%%%%%%% Skeleton %%%%%%%%%%%%%%%%%%
% Font settings
\usepackage[T1]{fontenc}
\usepackage{libertine}
\renewcommand*\ttdefault{lmvtt}
\usepackage{inconsolata}
% Margins
% Set to MSword style margins
\usepackage[margin=1in]{geometry}

% Section Formatting
\usepackage{titlesec}
\titleformat{\section}{\scshape\Large}{\thesection}{1em}{}[\titlerule]{}

%Caption formatting
\usepackage{caption}
\captionsetup[table]{skip=10pt}
\usepackage{float}
% Equations
\usepackage{amsmath}
\mathchardef\mhyphen="2D % Define a "math hyphen"
% Images
\usepackage[table,dvipsnames]{xcolor}
\usepackage{graphicx}

\usepackage[most]{tcolorbox}

% Figure labelling (inside figures)
\usepackage{stackengine}
\def\stackalignment{l}
% \topinset{label}{image}{from top}{from left}

% Textwrapped figures
\usepackage{wrapfig}
% \begin{wrapfigure}[numberLines]{placement}[overhang]{width}

% Tables
\usepackage{booktabs}
\usepackage{tabularx}
% Fix rowcolors for tabularx
\newcounter{tblerows}
\expandafter\let\csname c@tblerows\endcsname\rownum

% List settings
\usepackage{enumitem}
\setlist{itemsep=0em,parsep=0em}
%Change to alphabetical listing
%\begin{enumerate}[label=(\Alph*)]

% SI unit characters (includiong non-italic)
\usepackage{siunitx}
\usepackage{eurosym}
% Define Euro symbol
\DeclareSIUnit{\pers}{pers}
\DeclareSIUnit{\EUR}{\text{\euro}}
\sisetup{
  per-mode = symbol,
  inter-unit-product = \ensuremath{{}\cdot{}},
  range-phrase=--,
  range-units=single
}
\DeclareSIUnit \amphour {Ah} %Define Amphour

% Code blocks
\usepackage{listings}
\definecolor{gitGrey}{rgb}{0.97,0.97,0.97}
\definecolor{gitRed}{rgb}{0.86,0.09,0.28}
\definecolor{gitGreen}{rgb}{0.07,0.60,.18}
\definecolor{ruleGrey}{rgb}{0.8,0.8,0.8}
\lstdefinestyle{github}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  backgroundcolor = \color{Goldenrod!20},
  xleftmargin=0cm,
  extendedchars = true,
  showstringspaces=false,
  basicstyle=\ttfamily\small,
  keywordstyle=\ttfamily\small,
  commentstyle=\itshape\color{gitGreen},
  stringstyle=\color{gitRed},
  numbers=left,
  columns=fullflexible,
  tabsize=2}
  
% Hyper links
\usepackage[color links = true,
  all colors = blue]{hyperref}

% Hanging indents
\usepackage{hanging}

% Paragraph formatting
\usepackage{ragged2e}

%%%%%%%%%%%%%% Drawing %%%%%%%%%%%%%%%%%%%%
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{shapes}
\usetikzlibrary{patterns}
\usepgfplotslibrary{polar}
\pgfplotsset{compat=1.12}
%%%%%%%%%%%%%% Draft settings %%%%%%%%%%%%%
% Line numbering
\usepackage[switch, modulo]{lineno}
%\linenumbers

% Enable highlighting
\usepackage{soul}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Custom environments %%%%
\newenvironment{textbox}[1]
{
	\begin{tcolorbox}[float=b, width=\linewidth, boxsep=2pt,
		  arc=0pt, boxrule=1pt,
                  colback=gray!20, title=#1]
	  \raggedright}
{\end{tcolorbox}%\end{wraptbox}
}

%%%% Make R code enviroment
\lstnewenvironment{rcode}
{\lstset{style=github, language=R}}{}
%%%% Make bash code environment
\lstnewenvironment{bashcode}
{\lstset{style=github, 
language=bash,backgroundcolor=\color{Emerald!10}}}{}

\makeatletter
\DeclareCaptionFormat{form}{File\dotfill #3}
\captionsetup[lstlisting]{format=form,singlelinecheck=false,margin=0pt,
		font={tt},labelformat=empty,skip=0pt}
\newcommand{\rcodefile}[1]{
	\filename@parse{#1}
	\lstinputlisting[language=R, style=github,
	caption=\filename@base .\filename@ext]{#1}
}
\newcommand{\bashcodefile}[1]{
	\filename@parse{#1}
	\lstinputlisting[language=bash, style=github,
	caption=\filename@base]{#1}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amsfonts}
\title{{\Huge Acoustic Analysis}\\{\Large for Marine Biologists}}
\author{James Campbell}
\begin{document}
\maketitle
\frontmatter
\chapter{Preface}
This is a reference guide for common analytical methods applied to acoustic data.
We'll cover both the theoretical basis and practical methods which are commonly found in the marine acoustic literature.
The aim of this book is to be a comprehensive guide for biologists who are working with underwater acoustics.

Code blocks in this book are in the R programming language and depend on the packages: \textbf{ggplot2} and \textbf{ezPSD}.
ezPSD is not yet available on the CRAN (the R server network for distributing and archiving packages) so you'll instead have to install it via gitlab with the following commands.
Note, you'll need the package \textbf{devtools} to run the following block.

\begin{rcode}
require(devtools)
install_git("https://gitlab.com/RTbecard/ezPSD.git", build_vignettes = T, force = T)
require(ezPSD)
\end{rcode}

ezPSD is a package written specifically for underwater acoustic measurement and analysis.
To get an overview of what ezPSD can do, we suggest having a quick look at the basic use vignette.
You can access it with the following command one the package is installed.

\begin{rcode}
vignette(package = 'ezPSD',topic = 'basic-use')
\end{rcode}

In addition to R code, we also include bash scripts within a few topics.
bash scripting is used to automate commands on UNIX/LINUX machines.
bash scripts are widely compatible across linux and Mac computers and can also be used on windows 10 via the linux subsystem (we recommend using the popular ubuntu variant).
We'll distinguish bash from R code by changing the background colour like so:

\begin{bashcode}
	#!/bin/bash
	echo "hello world"
\end{bashcode}


\tableofcontents

% Start chapter numbering (leave preface section)
\mainmatter

\chapter{Measurement and Analysis}

\input{Chapters/MeasuringWithAHydrohpone.tex}

\chapter{Basic Formulas}\label{Chapter:BasicFormulas}
This chapter is intended as a quick reference guide for the underwater acoustic calculations commonly used by marine biologists.
It is recommended that you spend some time trying to understand how the formulas are derived, as this will reduce the chance that you make errors during your analysis.
The relevant R syntax for each equation is also given for the package \texttt{ezPSD}.

\input{Chapters/Formulas_and_Equations.tex}

\chapter{Simulating Acoustic Signals}

Only covering spectral domain for continuous sounds.

\begin{itemize}
	\item White noise with a specific SPL dB re 1uPa
	\item White noise with a specific SPL dB re 1uPa/Hz
	\item Boat spectra
\end{itemize}
\chapter{Understanding and Modelling Acoustic Propagation}
\input{Chapters/AcousticPropagation.tex}

\chapter{Discrete Signal Processing: Concepts and Practice}
\input{Chapters/DSP.tex}

\begin{itemize}
	\item FIR vs IIR fiters
	\item Laplace transform
	\item z transform
\end{itemize}

\section{Resampling}

\end{document}
