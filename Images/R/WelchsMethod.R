fs <- 400*2
n <- 100
freq <- 100
n <- length(y)
y <- cos(seq(from = 0,
             by = ((freq)/fs)*(pi*2),
             length.out = n)) + rnorm(n,sd = 0.4) 
plot(y, type = "l")
write.csv(data.frame(x = 1:n, y = y), row.names = F, file = "WelchsMethod.csv")

window.n <- n/2
window.segments <- data.frame(start = seq(from=1, to = n - window.n + 1, by = window.n/2),
                              end = seq(from=window.n, to = n, by = window.n/2))
hann <- sin(pi*(1:window.n)/window.n)^2; plot(hann)

template <- rep.int(NaN,n)
ts.windowd <- data.frame(x = 1:n,
                         apply(window.segments, MARGIN = 1,
                               FUN = function(x){
                                 out <- template
                                 out[x[1]:x[2]] <- y[x[1]:x[2]]*hann
                                 return(out)}))
names(ts.windowd)[2:4] <- c("1", "2", '3')

ts.windowd.2 <- data.frame(x = 1:window.n,
                           apply(window.segments, MARGIN = 1,
                                 FUN = function(x){
                                   out <- y[x[1]:x[2]]*hann
                                   return(out)}))
names(ts.windowd.2)[2:4] <- c("1", "2", '3')

freq <- seq(0,fs/2, length.out = ceiling(nrow(ts.windowd.2)/2))

fft <- data.frame(freq = freq)
for(i in 1:(ncol(ts.windowd.2) - 1)){
  y.fft <- abs(fft(ts.windowd.2[,i+1]))/window.n
  fft[,i+1] <- y.fft[1:(ceiling(window.n)/2)]*2
}
names(fft)[2:4] <- c("1", "2", '3')

fft$mean <- apply(fft[2:ncol(fft)], MARGIN = 1, FUN = "mean")
fft$k <- 0:(nrow(fft)-1)
fft$dB <- 20*log10((fft$mean/max(fft$mean)))

ts.windowd$y <- y
write.csv(ts.windowd, row.names = F, file = "WelchsMethodTS1.csv", na = "nan")
write.csv(ts.windowd.2, row.names = F, file = "WelchsMethodTS2.csv")
write.csv(fft, row.names = F, file = "WelchsMethodTS.csv")
