require(ggplot2)
require(ezPSD)

## Make simulated guassian noise
#### i.e. make a signal with flat spectrum
SPL <- 125
fs <- 10000
seconds <- 5
samples <- fs*seconds
signal <- rnorm(n = samples, sd = 10^(SPL/20))
## Test signal has correct SPL
20*log10(rms(signal))

## Make PSD
wl <- 2^10
plot.PSD <- ggpsd(ezWelch(signal, wl = wl)) + 
  ggtitle("Sim. Gaussian (5th, 50th, 95th prcntls)") + ylab("PSD (dB re 1\u03bcPa/Hz)")
## Make 1/3 octave plot
plot.octaves <- ezOctaveBands(signal, octaves.interval = 1/3,
                              freq.lim = c(10,5000))
x <- c(10,20,30,50,100,200,300,500,1000,3000)
x.minor <- c(40,60,70,80,90,400,600,700,800,900,4000, 2000)
plot.octaves <- ggplot(data = plot.octaves, aes(x = mid, y = 10*log10(power))) +
  geom_line() + theme_bw() + ylab("SPL (dB re 1\u03bcPa)") + 
  ggtitle("Sim. Gaussian (1/3 Octaves)") + geom_point() +
  scale_x_log10(name = "Frequency (Hz)", breaks = x, minor_breaks = x.minor)

## Show signals
ggsave(plot.PSD, width = 2, height = 1.5, filename = "GaussianPSD.jpeg",
       scale = 2, dpi = 'print')
ggsave(plot.octaves, width = 2, height = 1.5, filename = "GaussianSPL.jpeg",
       scale = 2, dpi = 'print')
